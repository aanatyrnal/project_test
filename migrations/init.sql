CREATE TYPE status_s AS ENUM ('requested', 'update', 'approved', 'declined');
CREATE TYPE gender_type AS ENUM ('female', 'male') ;

CREATE TABLE IF NOT EXISTS profiles(
    id                  serial primary key,
    email               text UNIQUE not null,
    surname             text not null,
    name                text not null,
    patronymic          text default null,
    date_of_birth       date not null,
    city                text not null,
    citizenship         text not null,

    passport_number     text not null,
    issuer              text not null,
    date_of_issue       date not null,
    subdivision_code    text not null,

    education           text not null,
    university          text not null,
    year_graduation     text not null,

    source              text not null,
    contact             text,
    text                text,
    create_at           timestamp default now(),
    update_at           timestamp default null
);
ALTER TABLE profiles ADD COLUMN status status_s ;
ALTER TABLE profiles ADD COLUMN comment text default null ;
ALTER TABLE profiles ADD COLUMN gender gender_type ;
ALTER TABLE profiles ADD COLUMN direction text ;
