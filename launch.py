from os import getenv
import asyncio

import uvicorn
from dotenv import load_dotenv

import app

load_dotenv()

if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    uvicorn.run(
        app=app.create_app(),
        host='127.0.0.1',
        port=int(getenv('API_SERVER_PORT'))
)
