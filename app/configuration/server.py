"""Server configuration."""
from typing import TypeVar

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.internal.pkg.middlewares.handle_http_exceptions import handle_api_exceptions, handle_internal_exception
from app.internal.routes import __routes__

__all__ = ["Server"]

from app.pkg.models.base import BaseAPIException

FastAPIInstance = TypeVar("FastAPIInstance", bound="FastAPI")


class Server:

    def __init__(self, app: FastAPI):
        self.__app = app
        self._register_routes(app)
        self._register_middlewares(app)
        self._register_http_exceptions(app)
        self.__register_cors_origins(app)

    def get_app(self) -> FastAPIInstance:
        return self.__app

    @staticmethod
    def _register_routes(app: FastAPIInstance) -> None:
        __routes__.allocate_routes(app)

    @staticmethod
    def __register_cors_origins(app: FastAPIInstance):
        """Register cors origins."""

        app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )

    @staticmethod
    def _register_middlewares(app: FastAPI):
        pass

    @staticmethod
    def _register_http_exceptions(app: FastAPIInstance):
        """Register http exceptions.

        FastAPIInstance handle BaseApiExceptions raises inside functions.

        Args:
            app: `FastAPI` application instance

        Returns: None
        """

        app.add_exception_handler(BaseAPIException, handle_api_exceptions)
        app.add_exception_handler(Exception, handle_internal_exception)
