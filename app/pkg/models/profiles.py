from datetime import date

from pydantic.fields import Field
from pydantic.types import PositiveInt

from .base import BaseModel

__all__ = [
    "ProfileFields",
    "Profile",
    "ProfileCreate",
    "ProfileUpdate",
    "ReadByUser",
]


class ProfileFields:
    id = Field(description="id", example="1")
    email = Field(description="email", example="email@email.com")
    surname = Field(description="Фамилия", example="Иванов")
    name = Field(description="Имя", example="Иван")
    patronymic = Field(description="Отчество", example="Иванович")
    date_of_birth = Field(description="Дата рождения", example="1990-05-05")
    city = Field(description="Город", example="Москва")
    citizenship = Field(description="Гражданство", example="РФ")
    passport_number = Field(description="Серия и номер паспорта", example="1111111111")
    issuer = Field(description="Кем выдан", example="ОФМС")
    date_of_issue = Field(description="Дата выдачи", example="2010-05-05")
    subdivision_code = Field(description="Код подразделения", example="040-010")

    education = Field(description="Уровень образования", example="Высшее")
    university = Field(description="Университет", example="МГУ")
    year_graduation = Field(description="Год окончания", example="2012")

    gender = Field(description='Пол', example="female")
    direction = Field(description='Желаемое направление', example="IT")

    source = Field(description="Источник", example="Платформа")
    contact = Field(description="Контакты", example="88008880000")
    text = Field(description="О себе", example="О себе")
    create_at = Field(description="create_at")
    update_at = Field(description="update_at")
    status = Field(description="Статус", example="requested")
    comment = Field(description="Комментарий админа", example="Комментарий")


class BaseProfile(BaseModel):
    """Base model for Profile"""


class Profile(BaseProfile):
    id: PositiveInt = ProfileFields.id
    email: str = ProfileFields.email
    surname: str = ProfileFields.surname
    name: str = ProfileFields.name
    patronymic: str = ProfileFields.patronymic
    date_of_birth: date = ProfileFields.date_of_birth
    city: str = ProfileFields.city
    citizenship: str = ProfileFields.citizenship
    passport_number: str = ProfileFields.passport_number
    issuer: str = ProfileFields.issuer
    date_of_issue: date = ProfileFields.date_of_issue
    subdivision_code: str = ProfileFields.subdivision_code

    education: str = ProfileFields.education
    university: str = ProfileFields.university
    year_graduation: str = ProfileFields.year_graduation

    gender: str = ProfileFields.gender
    direction: str = ProfileFields.direction

    source: str = ProfileFields.source
    contact: str = ProfileFields.contact
    text: str = ProfileFields.text
    create_at: date = ProfileFields.create_at
    update_at: date | None = ProfileFields.update_at
    status: str = ProfileFields.status
    comment: str | None = ProfileFields.comment


class ProfileCreate(BaseProfile):
    email: str = ProfileFields.email
    surname: str = ProfileFields.surname
    name: str = ProfileFields.name
    patronymic: str = ProfileFields.patronymic
    date_of_birth: date = ProfileFields.date_of_birth
    city: str = ProfileFields.city
    citizenship: str = ProfileFields.citizenship
    passport_number: str = ProfileFields.passport_number
    issuer: str = ProfileFields.issuer
    date_of_issue: date = ProfileFields.date_of_issue
    subdivision_code: str = ProfileFields.subdivision_code

    education: str = ProfileFields.education
    university: str = ProfileFields.university
    year_graduation: str = ProfileFields.year_graduation

    source: str = ProfileFields.source
    contact: str = ProfileFields.contact
    text: str = ProfileFields.text

    gender: str = ProfileFields.gender
    direction: str = ProfileFields.direction


class ReadByUser(BaseProfile):
    id: PositiveInt = ProfileFields.id


class ProfileUpdate(BaseProfile):
    id: PositiveInt = ProfileFields.id
    surname: str = ProfileFields.surname
    name: str = ProfileFields.name
    patronymic: str = ProfileFields.patronymic
    date_of_birth: date = ProfileFields.date_of_birth
    city: str = ProfileFields.city
    citizenship: str = ProfileFields.citizenship
    passport_number: str = ProfileFields.passport_number
    issuer: str = ProfileFields.issuer
    date_of_issue: date = ProfileFields.date_of_issue
    subdivision_code: str = ProfileFields.subdivision_code

    education: str = ProfileFields.education
    university: str = ProfileFields.university
    year_graduation: str = ProfileFields.year_graduation

    source: str = ProfileFields.source
    contact: str = ProfileFields.contact
    text: str = ProfileFields.text

    gender: str = ProfileFields.gender
    direction: str = ProfileFields.direction
