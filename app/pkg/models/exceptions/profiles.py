from fastapi import status

from app.pkg.models.base import BaseAPIException

__all__ = [
    "ProfileAlreadyExist",
    "ProfileNoContent"
]


class ProfileAlreadyExist(BaseAPIException):
    status_code = status.HTTP_409_CONFLICT
    message = "Profile already exist."


class ProfileNoContent(BaseAPIException):
    status_code = status.HTTP_204_NO_CONTENT
    message = "Not content"
