from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, status, Depends
from fastapi_pagination import Page, add_pagination, paginate

from app.internal import services
from app.pkg.models.profiles import *

router = APIRouter(prefix="/profile", tags=["Profile"])


@router.post(
    "/",
    response_model=Profile,
    status_code=status.HTTP_201_CREATED,
    summary="Create profile"
)
@inject
async def create(
        cmd: ProfileCreate,
        profile_service: services.ProfilesServices = Depends(Provide[services.Services.profiles])
):
    return await profile_service.create(cmd)


@router.get(
    "/{id:int}/",
    response_model=Profile,
    status_code=status.HTTP_200_OK,
    summary='Get Profiles user',
)
@inject
async def read(
        id: int,
        profile_service: services.ProfilesServices = Depends(Provide[services.Services.profiles])
):
    return await profile_service.read_by_user(cmd=ReadByUser(id=id))


@router.get(
    "/",
    response_model=Page[Profile],
    status_code=status.HTTP_200_OK,
    summary='Get all Profile user',
)
@inject
async def read(
        profile_service: services.ProfilesServices = Depends(Provide[services.Services.profiles])
):
    profile = await profile_service.read()
    return paginate(profile)
add_pagination(router)


@router.put(
    "/",
    response_model=Profile,
    status_code=status.HTTP_200_OK,
    summary="Update Profile user"
)
@inject
async def update(
        cmd: ProfileUpdate,
        profile_service: services.ProfilesServices = Depends(Provide[services.Services.profiles])
):
    return await profile_service.update(cmd)


@router.delete(
    "/",
    response_model=Profile,
    status_code=status.HTTP_200_OK,
    summary="Delete Profile user"
)
@inject
async def delete(
        id: int,
        profile_service: services.ProfilesServices = Depends(Provide[services.Services.profiles])
):
    return await profile_service.delete(cmd=ReadByUser(id=id))
