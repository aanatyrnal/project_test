"""Global point for collected routers."""

from app.internal.pkg.models import Routes
from app.internal.routes import profiles

__all__ = ["__routes__"]

__routes__ = Routes(
    routers=(
        profiles.router,
    ),
)
