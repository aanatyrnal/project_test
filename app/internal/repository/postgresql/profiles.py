from typing import List, Optional

from app.internal.repository.handlers.postgresql.collect_response import (
    collect_response,
)
from app.internal.repository.postgresql.connection import get_connection
from app.internal.repository.repository import Repository
from app.pkg import models

__all__ = ["ProfilesServices", ]


class ProfilesServices(Repository):
    @collect_response
    async def create(self, cmd: models.ProfileCreate) -> models.Profile:
        q = """
         insert into profiles(
             email, surname, name, patronymic, date_of_birth, city,
             citizenship, passport_number, issuer, date_of_issue,
             subdivision_code, education, university, year_graduation, 
             source, contact, text, create_at, status, gender, direction
         )
             values (
                 %(email)s, 
                 %(surname)s, 
                 %(name)s, 
                 %(patronymic)s, 
                 %(date_of_birth)s, 
                 %(city)s,
                 %(citizenship)s, 
                 %(passport_number)s, 
                 %(issuer)s, 
                 %(date_of_issue)s,
                 %(subdivision_code)s, 
                 %(education)s, 
                 %(university)s, 
                 %(year_graduation)s,
                 %(source)s, 
                 %(contact)s, 
                 %(text)s, 
                 now(),
                 'requested',
                 %(gender)s,
                 %(direction)s
             ) 
        returning 
            id, email, surname, name, patronymic, date_of_birth, city,
             citizenship, passport_number, issuer, date_of_issue,
             subdivision_code, education, university, year_graduation, 
             source, contact, text, create_at, status, comment, gender, direction;
    """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()

    @collect_response
    async def read_by_user(self, cmd: models.ReadByUser) -> models.Profile:
        q = """
            SELECT
                profiles.id, profiles.email, surname, name, patronymic, date_of_birth, city,
                citizenship, passport_number, issuer, date_of_issue, gender, direction,
                subdivision_code, education, university, year_graduation,
                source, contact, text, create_at, update_at, status, comment
            FROM profiles
            WHERE id = %(id)s ;
            """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()

    @collect_response
    async def read(self) -> List[models.Profile]:
        q = """
            SELECT
                profiles.id, profiles.email, surname, name, patronymic, date_of_birth, city,
                citizenship, passport_number, issuer, date_of_issue, gender, direction,
                subdivision_code, education, university, year_graduation,
                source, contact, text, create_at, update_at, status, comment
            FROM profiles;
            """
        async with get_connection() as cur:
            await cur.execute(q)
            return await cur.fetchall()

    @collect_response
    async def update(self, cmd: models.ProfileUpdate) -> models.Profile:
        q = """
            update profiles
                set
                    surname = %(surname)s,
                    name = %(name)s,
                    patronymic = %(patronymic)s,
                    date_of_birth = %(date_of_birth)s, 
                    city = %(city)s,
                    citizenship = %(citizenship)s,
                    passport_number = %(passport_number)s,
                    issuer = %(issuer)s,
                    date_of_issue = %(date_of_issue)s,
                    subdivision_code = %(subdivision_code)s,
                    education = %(education)s,
                    university = %(university)s,
                    year_graduation = %(year_graduation)s,
                    source = %(source)s,
                    contact = %(contact)s,
                    text = %(text)s,
                    update_at = now(),
                    status = 'update',
                    gender = %(gender)s,
                    direction = %(direction)s
                WHERE id = %(id)s 
            returning
                id, email, surname, name, patronymic, date_of_birth, city,
                citizenship, passport_number, issuer, date_of_issue,
                subdivision_code, education, university, year_graduation,
                source, contact, text, create_at, update_at, status, comment,
                gender, direction ;
        """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()

    @collect_response
    async def delete(self, cmd: models.ReadByUser) -> models.Profile:
        q = """
            delete from profiles
                where id = %(id)s
            returning
                id, email, surname, name, patronymic, date_of_birth, city,
                citizenship, passport_number, issuer, date_of_issue,
                subdivision_code, education, university, year_graduation,
                source, contact, text, create_at, update_at, status, comment, gender, direction ;
            """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()
