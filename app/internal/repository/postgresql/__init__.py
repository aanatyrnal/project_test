from dependency_injector import containers, providers

from .profiles import *

__all__ = ["Repository"]


class Repository(containers.DeclarativeContainer):
    profiles = providers.Factory(ProfilesServices)
