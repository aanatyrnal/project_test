from dependency_injector import containers, providers

from app.internal.repository.postgresql import Repository

from .profiles import *


class Services(containers.DeclarativeContainer):
    repository_container = providers.Container(Repository)

    profiles = providers.Factory(
        ProfilesServices,
        repository=repository_container.profiles
    )

