from typing import Optional, List

from app.internal.repository.postgresql import profiles
from app.internal.repository.repository import BaseRepository
from app.pkg import models
from app.pkg.models.exceptions.repository import EmptyResult, UniqueViolation
from app.pkg.models.exceptions.profiles import ProfileAlreadyExist, ProfileNoContent

__all__ = [
    "ProfilesServices",
]


class ProfilesServices:
    repository: profiles.ProfilesServices

    def __init__(self, repository: BaseRepository):
        self.repository = repository

    async def create(self, cmd: models.ProfileCreate) -> models.Profile:
        try:
            return await self.repository.create(cmd=cmd)
        except UniqueViolation:
            raise ProfileAlreadyExist

    async def read_by_user(self, cmd: models.ReadByUser):
        try:
            return await self.repository.read_by_user(cmd=cmd)
        except EmptyResult:
            raise ProfileNoContent

    async def read(self):
        try:
            return await self.repository.read()
        except EmptyResult:
            raise ProfileNoContent

    async def update(self, cmd: models.ProfileUpdate) -> models.Profile:
        return await self.repository.update(cmd=cmd)

    async def delete(self, cmd: models.ReadByUser) -> models.Profile:
        try:
            return await self.repository.delete(cmd=cmd)
        except EmptyResult:
            raise ProfileNoContent
